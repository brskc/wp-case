FROM node:20.3.0-alpine3.17 as build
WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run build

FROM node:20.3.0-alpine3.17

WORKDIR /app

COPY --from=build /app/package*.json ./
COPY --from=build /app/dist ./dist

RUN npm install --omit=production

CMD [ "npm", "run", "start:prod" ]
