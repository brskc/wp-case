
## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ yarn install
```

## Running the app (Docker)

```bash
$ docker-compose up --build
```

## Running the app
#### Edit .env file after
```bash
# development
$ yarn run start

# watch mode
$ yarn run start:dev

# production mode
$ yarn run start:prod
```


## Endpoints

```
{host}/api (swagger)

{host}/rooms
```