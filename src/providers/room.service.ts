import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Room } from '../entities/room.entity';
import { RoomMessage } from '../entities/room-message.entity';
import { User } from '../entities/user.entity';

@Injectable()
export class RoomService {
  constructor(
    @InjectRepository(Room)
    private readonly roomRepository: Repository<Room>,
    @InjectRepository(RoomMessage)
    private readonly roomMessageRepository: Repository<RoomMessage>,
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getAllRooms() {
    return await this.roomRepository
      .createQueryBuilder('room')
      .leftJoin(
        (subQuery) => {
          return subQuery
            .select('rm.roomId, MAX(rm.id)', 'maxId')
            .from(RoomMessage, 'rm')
            .where('rm.isDeleted = 0')
            .groupBy('rm.roomId');
        },
        'subquery',
        'room.id = subquery.roomId and room.isDeleted = 0',
      )
      .leftJoinAndMapOne(
        'room.lastMessage',
        RoomMessage,
        'lastMessage',
        'lastMessage.id = subquery.maxId and lastMessage.isDeleted = 0',
      )
      .leftJoinAndMapOne(
        'lastMessage.receiver',
        User,
        'receiver',
        'receiver.id = lastMessage.receiverId',
      )
      .leftJoinAndMapOne(
        'lastMessage.sender',
        User,
        'sender',
        'sender.id = lastMessage.senderId',
      )
      .orderBy('lastMessage.createdAt', 'DESC')
      .select([
        'room.id',
        'lastMessage.content',
        'lastMessage.createdAt',
        'sender.id',
        'sender.fullName',
        'sender.photo',
        'receiver.id',
        'receiver.fullName',
        'receiver.photo',
      ])
      .getMany();
  }
}
