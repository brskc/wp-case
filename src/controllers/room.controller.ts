import { Controller, Get } from '@nestjs/common';
import { RoomService } from '../providers/room.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('Rooms')
@Controller('rooms')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Get()
  async getAllRooms() {
    const rooms = await this.roomService.getAllRooms();
    return { rooms };
  }
}
