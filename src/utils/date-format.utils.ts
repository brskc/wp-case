import { ValueTransformer } from 'typeorm';
import { format } from 'date-fns';

export class DateTransformer implements ValueTransformer {
  to(value: Date): string {
    return value ? format(value, 'yyyy-MM-dd HH:mm:ss') : null;
  }
  from(value: Date): string {
    return value ? format(value, 'yyyy-MM-dd HH:mm:ss') : null;
  }
}
