import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  OneToOne,
} from 'typeorm';
import { RoomMessage } from './room-message.entity';

@Entity()
export class Room {
  @PrimaryGeneratedColumn()
  id: number;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  createdAt: Date;

  @Column({ default: false })
  isDeleted: boolean;

  @OneToOne(() => RoomMessage, (message) => (message.receiver, message.sender))
  message: RoomMessage;
}
